package com.example.demo;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class UserRepositoryIntegrationTest {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void whenFindByEmail_thenReturnUser() {
        String email = "sauvageb@gmail.com";
        // Given
        User user = new User("sauvageb", email, "qwerty");
        userRepository.save(user);

        // When
        User found = userRepository.findByEmail(email);

        // Then
        assertThat(found.getEmail()).isEqualTo(user.getEmail());
        assertThat(found.getUsername()).isEqualTo(user.getUsername());
    }
}
